A simple and easy-to-use banking application built using the Go programming language.
Introduction

This repository contains the source code for a Banking App which is a simple and easy-to-use application for managing personal finances. The app provides functionality for creating bank accounts, performing transactions (deposit, withdrawal), and checking the account balance.
Features

    Create bank accounts
    Deposit money into an account
    Withdraw money from an account
    Check the account balance
    Transfer money between accounts

Prerequisites

    Go version 1.14 or higher
    Git installed on your system

Installation

Clone the repository to your local machine:

shell

$ git clone https://gitlab.com/M.darvish/banking-app.git

Change into the directory:

shell

$ cd banking-app

Build the binary:

go

$ go build

Run the binary:

shell

$ ./banking-app

Usage

Follow the on-screen instructions to perform different actions in the app.
Contributing

Contributions are welcome! If you want to contribute to the project, please fork the repository, make your changes and submit a pull request.
License

This project is licensed under the MIT License - see the LICENSE file for details.